<img alt="Eye am alien, Eye need space !!!" src="./static/expedition.gif" width="100%"/>

```shell

  ██╗    ██╗███████╗     █████╗ ██████╗ ███████╗    ██╗   ██╗ ██████╗ ██████╗ ████████╗███████╗██╗  ██╗
  ██║    ██║██╔════╝    ██╔══██╗██╔══██╗██╔════╝    ██║   ██║██╔═══██╗██╔══██╗╚══██╔══╝██╔════╝╚██╗██╔╝
  ██║ █╗ ██║█████╗      ███████║██████╔╝█████╗      ██║   ██║██║   ██║██████╔╝   ██║   █████╗   ╚███╔╝
  ██║███╗██║██╔══╝      ██╔══██║██╔══██╗██╔══╝      ╚██╗ ██╔╝██║   ██║██╔══██╗   ██║   ██╔══╝   ██╔██╗
  ╚███╔███╔╝███████╗    ██║  ██║██║  ██║███████╗     ╚████╔╝ ╚██████╔╝██║  ██║   ██║   ███████╗██╔╝ ██╗
   ╚══╝╚══╝ ╚══════╝    ╚═╝  ╚═╝╚═╝  ╚═╝╚══════╝      ╚═══╝   ╚═════╝ ╚═╝  ╚═╝   ╚═╝   ╚══════╝╚═╝  ╚═╝

```
[![last.fm last played](https://img.shields.io/endpoint?url=https://workers.vortex.name/lastfm/endpoint.json)](https://www.last.fm/user/zero-vortex)

![config update](https://github.com/0-vortex/0-vortex/workflows/config/badge.svg)
 ![quote of the day](https://github.com/0-vortex/0-vortex/workflows/quote%20of%20the%20day/badge.svg)
 ![wakatime stats](https://github.com/0-vortex/0-vortex/workflows/wakatime%20stats/badge.svg)

![0-vortex Actions](https://api.meercode.io/badge/0-vortex/0-vortex?type=ci-count&lastDay=31)

<!--STARTS_HERE_QUOTE_README-->
<i>❝The first 1GB hard disk drive was announced in 1980 which weighed about 550 pounds, and had a price tag of $40,000.❞</i>
<!--ENDS_HERE_QUOTE_README-->

### :crystal_ball: Start a conversation

- Linkedin: [tedvortex](https://www.linkedin.com/in/tedvortex)
- Twitter: [0_vortex](https://twitter.com/0_vortex)
- Psychology: [commander personality](https://www.16personalities.com/profiles/bdbc4d20d6087)
- Email: [ted.vortex@gmail.com](mailto:ted.vortex@gmail.com?subject=Contact%20Request)
- Phone: [754-345-624](sms:+40754345624)

### :chart_with_upwards_trend: Development Activity Metrics

<!--START_SECTION:waka-->
![Profile Views](http://img.shields.io/badge/Profile%20Views-7-blue)

![Lines of code](https://img.shields.io/badge/From%20Hello%20World%20I%27ve%20Written-629695%20lines%20of%20code-blue)

**🐱 My Github Data** 

> 🏆 778 Contributions in the Year 2021
 > 
> 📦 156.2 kB Used in Github's Storage 
 > 
> 💼 Opted to Hire
 > 
> 📜 50 Public Repositories 
 > 
> 🔑 5 Private Repositories  
 > 
**I'm a Night 🦉** 

```text
🌞 Morning    94 commits     ███░░░░░░░░░░░░░░░░░░░░░░   14.24% 
🌆 Daytime    118 commits    ████░░░░░░░░░░░░░░░░░░░░░   17.88% 
🌃 Evening    136 commits    █████░░░░░░░░░░░░░░░░░░░░   20.61% 
🌙 Night      312 commits    ███████████░░░░░░░░░░░░░░   47.27%

```
📅 **I'm Most Productive on Sunday** 

```text
Monday       83 commits     ███░░░░░░░░░░░░░░░░░░░░░░   12.58% 
Tuesday      80 commits     ███░░░░░░░░░░░░░░░░░░░░░░   12.12% 
Wednesday    94 commits     ███░░░░░░░░░░░░░░░░░░░░░░   14.24% 
Thursday     94 commits     ███░░░░░░░░░░░░░░░░░░░░░░   14.24% 
Friday       66 commits     ██░░░░░░░░░░░░░░░░░░░░░░░   10.0% 
Saturday     121 commits    ████░░░░░░░░░░░░░░░░░░░░░   18.33% 
Sunday       122 commits    ████░░░░░░░░░░░░░░░░░░░░░   18.48%

```


📊 **This Week I Spent My Time On** 

```text
💬 Programming Languages: 
sh                       11 hrs 49 mins      ██████████░░░░░░░░░░░░░░░   43.13% 
JSON                     3 hrs 31 mins       ███░░░░░░░░░░░░░░░░░░░░░░   12.89% 
JavaScript               3 hrs 17 mins       ███░░░░░░░░░░░░░░░░░░░░░░   12.0% 
Markdown                 3 hrs 10 mins       ███░░░░░░░░░░░░░░░░░░░░░░   11.59% 
YAML                     2 hrs 7 mins        ██░░░░░░░░░░░░░░░░░░░░░░░   7.76%

🔥 Editors: 
WebStorm                 13 hrs 57 mins      ████████████░░░░░░░░░░░░░   50.92% 
Zsh                      11 hrs 49 mins      ██████████░░░░░░░░░░░░░░░   43.13% 
Atom                     1 hr 37 mins        █░░░░░░░░░░░░░░░░░░░░░░░░   5.95%

🐱‍💻 Projects: 
open-sauced              4 hrs 48 mins       ████░░░░░░░░░░░░░░░░░░░░░   17.52% 
explore.opensauced.pizza 3 hrs 57 mins       ███░░░░░░░░░░░░░░░░░░░░░░   14.43% 
open-sauced-semantic-conf3 hrs 35 mins       ███░░░░░░░░░░░░░░░░░░░░░░   13.11% 
frontend                 3 hrs 28 mins       ███░░░░░░░░░░░░░░░░░░░░░░   12.71% 
Terminal                 2 hrs 31 mins       ██░░░░░░░░░░░░░░░░░░░░░░░   9.19%

```


 Last Updated on 03/08/2021
<!--END_SECTION:waka-->

### :closed_lock_with_key: Machines only section

<details>
  <summary>Hard facts about life</summary>

> 4e6f7420656e6c69676874656e65642c206a7573742062726f6b656e2e
>
> 48617070696e657373206973207265616c69747920776974686f7574206578706563746174696f6e2e
>
> 4275742074686572652773207374696c6c206e6f207265737420666f7220746865207769636b65642e

</details>

<details>
  <summary>Memes</summary>

![](./static/meme-this-is-fine.png?raw=true " ")
![](./static/meme-wisdom-of-the-ancients.png?raw=true " ")
![](./static/meme-full-it-replacement.png?raw=true " ")
![](./static/meme-burnout.png?raw=true " ")
![](./static/meme-adrs.png?raw=true " ")
![](./static/meme-flutter.png?raw=true " ")
![](./static/meme-c-plus-plus.png?raw=true " ")
![](./static/meme-cats.png?raw=true " ")
![](./static/meme-corona.png?raw=true " ")
![](./static/meme-devops.png?raw=true " ")
![](./static/meme-devs.png?raw=true " ")
![](./static/meme-don-t-argue-with-angry-ninjas.png?raw=true " ")
![](./static/meme-java.png?raw=true " ")
![](./static/meme-never-stop-dreaming.png?raw=true " ")
![](./static/meme-otaku.png?raw=true " ")
![](./static/meme-terminal-designers.png?raw=true " ")
![](./static/meme-threads.png?raw=true " ")

</details>
